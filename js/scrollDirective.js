app.directive("scroll", function ($window) {
    return function(scope, element, attrs) {
        angular.element($window).bind("scroll", function() {
             if (this.pageYOffset >= 100) {
                 element.addClass('hideDownArrow');
                 console.log('Scrolled below header.');
             } else {
                 element.removeClass('hideDownArrow');
                 console.log('Header is in view.');
             }
        });
    };
});
