app.controller('homeCtrl', ['$scope', function($scope){
  $scope.scrollTo = function(id) {
      var old = $location.hash();
      $location.hash(id);
      $anchorScroll();
      $location.hash(old);
  };
}]);
