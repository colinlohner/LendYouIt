app.controller('listingCtrl', ['$scope','$rootScope', 'distanceService', '$firebaseArray', '$firebaseObject', 'listingRef', function($scope,$rootScope, distanceService, $firebaseArray, $firebaseObject, listingRef){
  var ad = $firebaseObject(listingRef);
  ad.$bindTo($scope, 'ad');
}]);
