app.controller('newListingCtrl', ['$scope', '$firebaseArray', '$rootScope', '$state', function($scope, $firebaseArray,$rootScope, $state){

  if(!$rootScope.loggedin){
    $state.go('home');
  }

  var ref = new Firebase('https://lendyouit.firebaseio.com/ads');

  $scope.ads = $firebaseArray(ref);

  $scope.newListing = {};

  $scope.createListing = function(){
    $scope.ads.$add({
      adName: $scope.newListing.listingName,
      adLocation: $scope.newListing.listingLocation,
      type: $scope.newListing.type,
      price: $scope.newListing.price,
      length: $scope.newListing.length,
      description: $scope.newListing.description,
      user: $rootScope.currentUserData.displayName
    });
    $scope.newListing = {};
    $state.go('listings');
  };
}]);
