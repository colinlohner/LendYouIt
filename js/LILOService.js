app.service('LILOService', ['$firebaseAuth', '$rootScope', '$firebaseArray', function($firebaseAuth, $rootScope, $firebaseArray){
  var ref = new Firebase('https://lendyouit.firebaseio.com/users');

  var auth = $firebaseAuth(ref);
  var users = $firebaseArray(ref);

  $rootScope.loggedin = false;
  var accountActive = false;
  $rootScope.accountAction = "Login/Sign up";
  $rootScope.currentUserData = {};

  this.login = function(){
    if(accountActive === true){
      accountActive = false;
      $rootScope.loggedin = false;
      $rootScope.logginData = {};
      $rootScope.currentUserData = {};
      $rootScope.accountAction = "Login/Sign up";
      ref.unauth();
      $rootScope.$broadcast('user.loggedout');
    }else{
      auth.$authWithOAuthPopup("google").then(function(authData){
        var userAlreadyExists = false;
        $rootScope.logginData = authData;
        for (var i = 0; i < users.length; i++) {
          if(users[i].googleid === authData.google.id){
            userAlreadyExists = true;
          }
        }
        if(!userAlreadyExists){
          users.$add({
            googleid: $rootScope.logginData.google.id,
            displayName: $rootScope.logginData.google.displayName,
            profileImg: $rootScope.logginData.google.profileImageURL,
            gender: $rootScope.logginData.google.cachedUserProfile.gender,
            firstName: $rootScope.logginData.google.cachedUserProfile.given_name,
            lastName: $rootScope.logginData.google.cachedUserProfile.family_name,
            fullName: $rootScope.logginData.google.cachedUserProfile.name
          });
        }
        accountActive = true;
        $rootScope.loggedin = true;
        for (var j = 0; j < users.length; j++) {
          if(users[j].googleid === authData.google.id){
            $rootScope.currentUserData = users[j];
          }
        }
        $rootScope.$broadcast('user.loggedin');
      }).catch(function(error){
        alert('Login Failed, please try again...');
        console.log('failed');
      });
    }

  };

}]);
