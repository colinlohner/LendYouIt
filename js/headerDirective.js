app.directive('headerDirective', function(){
  return {
    restrict: 'E',
    templateUrl: 'js/templates/headerDirectiveTemplate.html',
    controller:function($scope, LILOService,$firebaseAuth, $rootScope){
      $scope.menuActive = false;
      $scope.menuIcon = 'fa fa-bars';
      $scope.accountImage = $rootScope.currentUserData.profileImg;
      $scope.user = $rootScope.currentUserData.displayName;

      $scope.toggleMenu = function(){
        if($scope.menuActive){
          $scope.menuIcon = 'fa fa-bars';
        }else{
          $scope.menuIcon = 'fa fa-times';
        }
        $scope.menuActive = !$scope.menuActive;
      };

      $scope.login = function(){
        LILOService.login();
        $scope.accountAction = $rootScope.accountAction;
      };
      $scope.$on('user.loggedin', function(){
        $scope.accountAction = $rootScope.accountAction;
        $scope.accountImage = $rootScope.currentUserData.profileImg;
        $scope.user = $rootScope.currentUserData.displayName;
      });
      $scope.$on('user.loggedout', function(){
        $scope.accountAction = $rootScope.accountAction;
        $scope.accountImage = '';
        $scope.user = "Logging you out...";
      });
      $scope.items = [{
        name: "Action"
      }, {
        name: "Another action"
      }, {
        name: "Something else here"
      }];

    },
    link: function(scope, element, attrs){
      console.log(scope.menuActive);
      element.find('a').on('click', function(){
        scope.toggleMenu();
      });
    }
  };
});
