var app = angular.module('lendYouIt', ['ui.router', 'ngAnimate', 'firebase', 'nsPopover']);

app.config(function($stateProvider, $urlRouterProvider){
  $urlRouterProvider.otherwise('/home');

  $stateProvider.state('home', {
    url: '/home',
    controller: 'homeCtrl',
    templateUrl: 'js/templates/home.html'
  }).state('listings', {
    url: '/listings',
    controller: 'listingsCtrl',
    templateUrl: 'js/templates/listings.html'
  }).state('newListing', {
    url: '/newListing',
    controller: 'newListingCtrl',
    templateUrl: 'js/templates/newListing.html'
  }).state('account', {
    url: '/account',
    controller: 'accountCtrl',
    templateUrl: 'js/templates/account.html'
  }).state('listing', {
    url: '/listings/listing/:listingId',
    controller: 'listingCtrl',
    templateUrl: 'js/templates/listing.html',
    resolve: {
      listingRef: function(listingService, $stateParams){
        return listingService.getListing($stateParams.listingId);
      }
    }
  });
});
