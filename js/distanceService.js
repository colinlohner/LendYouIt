app.factory('distanceService', function($http, $q){
  var factory = {};
  function distance(lat1, lon1, lat2, lon2) {
  	var radlat1 = Math.PI * lat1/180;
  	var radlat2 = Math.PI * lat2/180;
  	var theta = lon1-lon2;
  	var radtheta = Math.PI * theta/180;
  	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
  	dist = Math.acos(dist);
  	dist = dist * 180/Math.PI;
  	dist = dist * 60 * 1.1515;
  	return dist;
  }
  var getUserLocation = function(){
    navigator.geolocation.getCurrentPosition(function(pos) {
      var currentLocation = {
        lon: pos.coords.longitude,
        lat: pos.coords.latitude
      };
      return currentLocation;
    }, function(error) {
      alert('Unable to get your location');
    });
  };
  var zipToLL = function(zipcode){
    var dfd = $q.defer();
      $http.get('https://maps.googleapis.com/maps/api/geocode/json?address='+ zipcode +'&key=AIzaSyBzVYn6K5Dx_xdmnO_lYLgESoXPrht3TvA').then(function(res){
        var results = res.data.results[0].geometry.location;
        dfd.resolve(results);
      });
    return dfd.promise;
  };
  factory.getDistance = function(zipcode){
    // var dfd1 = $q.defer();
    // var dfd2 = $q.defer();
    var zipLL = zipToLL(zipcode);
    // var userLocation = getUserLocation();
    var user = zipToLL(84604);
    var promArr = [zipLL, user];

    $q.all(promArr).then(function(){
      var distanceFrom = distance(user.lat, user.lng, zipLL.lat, zipLL.lng);
      console.log(distanceFrom);
      return distanceFrom;
    });
  };
  return factory;
});
