app.directive('footerDirective', function(){
  return {
    restrict: 'E',
    templateUrl: 'js/templates/footerDirectiveTemplate.html',
    link: function(scope, element, attrs){
      var currentTime = new Date();
      scope.time = currentTime.toString();
    }
  };
});
